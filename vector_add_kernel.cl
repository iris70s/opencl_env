#pragma OPENCL EXTENSION cl_arm_printf : enable

__kernel void vector_add(__global int *A, __global int *B, __global int *C) {

    // Get the index of the current element
    int i = get_global_id(0);

    // Do the operation
    // printf( "Hello from work item %lu!\n", (ulong) get_global_id(0) );
  	C[i] = A[i] - B[i];
}
