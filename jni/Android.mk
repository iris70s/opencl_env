LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := libgles_mali
#LOCAL_SRC_FILES := libs/libopencl.a
LOCAL_SRC_FILES := libs/libGLES_mali.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := opencl_example
LOCAL_C_INCLUDES := include/CL/cl.h
LOCAL_SRC_FILES += src/main.c
LOCAL_SHARED_LIBRARIES := libgles_mali
LOCAL_CFLAGS += -Wall -fPIE
LOCAL_CFLAGS += -Wno-implicit-function-declaration
include $(BUILD_EXECUTABLE)
