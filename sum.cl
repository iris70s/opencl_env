#pragma OPENCL EXTENSION cl_arm_printf : enable

__kernel void vector_add(__global ulong *val) {

  size_t i = get_global_id(0);

  for(ulong j=0; j<10000000; j++)
	{
		printf("j=%d    i=%d\n", j, i);
    val[i] += j; //((val[i]+2) % (val[i]+1));
  }
}
